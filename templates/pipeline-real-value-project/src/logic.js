let { Model } = require( 'real-value-lang' )

let debug =  require('debug')('pipeline')

function provisionSystem () {
  let model = new Model()

  return {
    model
  }
}

function reduceToView(h,n){
  return {
    reduced: null,
    downstream: [n]
  }
}

function provisionProcessing (architecture,timesheetStream) {

  const viewStream = timesheetStream.reduce(reduceToView)

  return {
    ...architecture,
    timesheetStream,

    viewStream
  }
}

module.exports= {
  provisionProcessing,
  provisionSystem
}
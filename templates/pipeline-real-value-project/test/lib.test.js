import { describe } from 'riteway'

import { provisionSystem, provisionProcessing } from '../index.js'

import Debug from 'debug'
let debug = Debug('pipeline.test')

describe('Use Logic', async (assert) => {

  debug('here')
  let architecture = provisionSystem()

  let timesheetStream = architecture.model.from('./test/timesheet.csv')

  architecture = provisionProcessing(architecture,timesheetStream)

  architecture.viewStream.log()

  await architecture.model.run(() => {
    assert({
      given: 'the source',
      should: 'stream events',
      actual: true,
      expected: true
    })
  })
})

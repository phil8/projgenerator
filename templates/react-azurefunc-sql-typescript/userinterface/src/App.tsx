import React, { Suspense } from 'react';
import { HashRouter, Switch, Route } from "react-router-dom"
import { Layout } from './layout'
import Home from './pages/Home.page'
import Configuration from './pages/Configuration.page'
import Scenarios from './pages/Scenarios.page'
import { Loader } from './components'
import style from './styles/Page.module.sass';

const PlaceholderDatasetSelection = (props:any)=>{
  const {selectedDistribution}=props
  return <select name="placeholder" id="placeholder" onChange={(event)=>{selectedDistribution(event.target.value);event?.preventDefault()}}>
    <option value="">---Selection a dataset---</option>
    <option value="Distribution1">Distribution1</option>
    <option value="Distribution2">Distribution2</option>
    <option value="Distribution3">Distribution3</option>
  </select>
}

function App() {
  return <Suspense fallback={<Loader />}>
          <HashRouter hashType='slash'>
            <Layout>
              <Switch>                                                
                <Route path="/scenarios" render={() => <Scenarios style={style} />} />
                <Route path="/configuration/:scenarioId" render={(props:any) => {                  
                  return <Configuration style={style} DatasetSelection={PlaceholderDatasetSelection} scenarioId={props.match.params.scenarioId}/> 
                }} />
                <Route path="/configuration" render={() => <Configuration style={style} DatasetSelection={PlaceholderDatasetSelection}/>}  />                
                <Route path="/" exact component={Home} />
              </Switch>    
              </Layout>
            </HashRouter>
          </Suspense>
}

export default App; 
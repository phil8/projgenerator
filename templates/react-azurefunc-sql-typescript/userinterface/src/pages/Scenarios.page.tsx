import React, { FC, useEffect, useState } from 'react';
import { queryScenarios  } from '../contract/api';
import {IScenario} from '../../../application/src/types'

interface IPageProps {
    style: any,
}

const Scenarios: FC<IPageProps> = (props: IPageProps) => {

    const {style} = props
    const [scenarios,setScenarios] = useState<IScenario[]>([])

    useEffect(()=>{
        async function getData(){
            const modelScenarios = await queryScenarios()
            setScenarios(modelScenarios)
        }
        getData()
    },[])

    return <div className={style.pageContainer}>
        <div className={style.pageTitle}>Scenario</div>
        <div className={style.pageContent}>            
            {scenarios                
                .map((scenario:any)=><div key={scenario.id}>
                        <a href={`/#/configuration/${scenario.id}`}>{scenario.name}</a>
                    </div>
            )}
        </div>
    </div>
}

export default Scenarios
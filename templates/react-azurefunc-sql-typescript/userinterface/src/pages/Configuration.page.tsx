import React, { FC, useEffect, useState } from 'react';
import { queryModelSpecification, createModelScenario, getScenario } from '../contract/api';
import {reifyScenario} from '../../../application/src/service/UseCaseService'
import {IDomain,IDomainValue} from '../../../application/src/types'
import {CONSTANT_TYPE,RANGE_TYPE,SET_TYPE,DISTRIBUTION_TYPE,DISTRIBUTIONSET_TYPE} from '../../../application/src/domain'

interface IDatasetSelectionProps {
    style: any,
    parameterName: string,
    selectedDistribution: (name:string)=>void,
}

interface IPageProps {
    style: any,
    scenarioId?: any,
    DatasetSelection: FC<IDatasetSelectionProps>,
}

const Configuration: FC<IPageProps> = (props: IPageProps) => {

    const {style, scenarioId,DatasetSelection} = props
    const [specification,setSpecification] = useState<any>([])
    const [scenario,setScenario] = useState<any>([])

    const onChangeDomainValue =(domainvalue:IDomainValue,value:string) => {        
            domainvalue.value=value        
            setScenario([...scenario])
    }

    const DomainConstant = (props:any) => {
        const {style,domain,domainValue,setDomainValue} = props
        const [value,setValue]=useState<string>(domainValue.value)
        const onChange = (event:any)=>{setValue(event.target.value)}
        const onKey = (event:any) => {if(event.code==='Enter')onChangeDomainValue(domainValue,value)}
        return <input name={domain.name} id={domainValue.id} onChange={onChange} onKeyDown={onKey}></input>
    }
    
    const DomainRange = (props:any) => {
        const {style,domain,domainValue} = props
        const [value,setValue]=useState<string>(domainValue.value)
        const [min,max] = value.split(',')        
        const onMinChange = (event:any)=>{setValue(`${event.target.value},${max}`)}
        const onMaxChange = (event:any)=>{setValue(`${min},${event.target.value}`)}
        const onKey = (event:any) => {if(event.code==='Enter')onChangeDomainValue(domainValue,value)}
        return <div>
            Min:<input name={domain.name} id={`${domainValue.id}-min`} onChange={onMinChange} onKeyDown={onKey} value={min}></input>
            Max:<input name={domain.name} id={`${domainValue.id}-max`} onChange={onMaxChange} onKeyDown={onKey} value={max}></input>
        </div>
    }
    
    const DomainSet = (props:any) => {
        const {style,domain,domainValue} = props    
        return <select name={domain.name} id={domainValue.id} onChange={(event)=>{onChangeDomainValue(domainValue,event.target.value);event.preventDefault()}}>
            {domain.setitems.map((item:any)=><option key={item.value} value={item.value} >{item.value}</option>)}
        </select>
    }

    const DomainDistribution = (props:any) => {
        const {style,domain,domainValue} = props
        const [value,setValue]=useState<string>(domainValue.value)
        const [distribution,parameters] = value.split(':')
        const onChangeDistribution = (event:any)=>{                        
            onChangeDomainValue(domainValue,`${event.target.value}:${parameters}`)
            event.preventDefault();
        }
        const onChangeParameters = (event:any)=>{setValue(`${distribution}:${event.target.value}`);event.preventDefault();}
        const onKey = (event:any) => {if(event.code==='Enter')onChangeDomainValue(domainValue,value)}
        return <div>
            <select name={domain.name} id={domainValue.id} onChange={onChangeDistribution} value={distribution}>
                <option key={`${domainValue.id}-Normal`} value="Normal">Normal</option>
                <option key={`${domainValue.id}-Beta`} value="Beta">Beta</option>
            </select>
            <input name={`${domainValue.id}-parameters`} id={`${domain.id}-parameters`} onChange={onChangeParameters} onKeyDown={onKey} value={parameters}></input>
        </div>
    }

    const DomainDistributionSet = (props:any) => {
        const {style,parameter,domain,domainValue} = props
        const onSelect = (value:string) => {
            console.log(value)
            onChangeDomainValue(domainValue,value)
        }
        return <DatasetSelection style={style} parameterName={parameter.name} selectedDistribution={onSelect} />
    }
    
    const DomainValue = (props:any) => {
        const {style,domainvalue,parameter} = props
        const domain = parameter.domains.find((d:any)=>d.id === domainvalue.domainId)        
        return <div key={domainvalue.id} className={style.domain}>
                    <div>
                        DomainValue:{domainvalue.value}
                    </div>
                    {parameter.domains.map((domain:IDomain)=>{
                        return <div key={domain.id} className={style.domainType}>
                        DomainType: {domain.domainType}
                        {domain.domainType===SET_TYPE && <DomainSet style={style} domain={domain} domainValue={domainvalue}/>}
                        {domain.domainType===CONSTANT_TYPE && <DomainConstant style={style} domain={domain} domainValue={domainvalue}/>}
                        {domain.domainType===RANGE_TYPE && <DomainRange style={style} domain={domain} domainValue={domainvalue}/>}
                        {domain.domainType===DISTRIBUTION_TYPE && <DomainDistribution style={style} domain={domain} domainValue={domainvalue}/>}
                        {domain.domainType===DISTRIBUTIONSET_TYPE && <DomainDistributionSet style={style} domain={domain} domainValue={domainvalue} parameter={parameter}/>}
                    </div>
                    })}
                </div>
    }
    
    const ParameterInstance = (props:any) => {
        const {style,parameterInstance,component} = props

        const parameter = component.parameters.find((si:any)=>si.id === parameterInstance.parameterId)
        const domainValues = scenario.filter((si:any)=>si.type==='DomainValue' && si.parameterinstanceId===parameterInstance.id)
        return <div key={parameterInstance.id} className={style.parameter}>
            ParameterInstance:{parameterInstance.name}            
            {domainValues.map((domainvalue:any)=><DomainValue style={style} key={domainvalue.id} domainvalue={domainvalue} parameter={parameter}/>)}
            </div>
    }
    
    const ComponentInstance = (props:any) => {
        const {style,componentInstance} = props
        const component = specification.find((comp:any)=>comp.id===componentInstance.modelComponentId)
        const parameterInstances = scenario.filter((si:any)=>si.type==='ParameterInstance' && si.modelcomponentinstanceId===componentInstance.id)        
        return component 
                ? 
                <div key={component.id} className={style.component}>
                ModelComponentInstance:{componentInstance.name}
                {parameterInstances.map((parameterInstance:any)=><ParameterInstance style={style} key={parameterInstance.id} parameterInstance={parameterInstance} component={component}/>)}
                </div> 
                :
                <div>Component [{componentInstance.componentId}] not found</div>
    }

    useEffect(()=>{
        async function getData(){
            const modelSpecification = await queryModelSpecification()
            setSpecification(modelSpecification)
            if(!scenarioId){
                const modelScenario = await reifyScenario(modelSpecification)
                //console.log(modelScenario)
                setScenario(modelScenario)            
            }else {
                const modelScenario = await getScenario(scenarioId)
                //console.log(modelScenario)
                setScenario(modelScenario)  
            }
        }
        getData()
    },[])

    const handleButton = ()=>{        
        createModelScenario(scenario)
    }

    return <div className={style.pageContainer}>
        <div className={style.pageTitle}>Scenario</div>
        <div className={style.pageContent}>
            <button onClick={handleButton}>Save</button>             
            {scenario
                .filter((si:any)=>si.modelComponentId)
                .map((compinstance:any)=><ComponentInstance style={style} key={compinstance.id} componentInstance={compinstance}/>
            )}
        </div>
    </div>
}

export default Configuration
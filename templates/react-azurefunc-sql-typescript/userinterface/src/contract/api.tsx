import { getAPI, getAPIDomain } from '../config';

const handleResponse = (res:Response) => {
  if (res.status === 401) {
    window.location.href = `/`
  } else if (res.status === 500) {
      return null
  } else {
    return (res!== null) ? res.json() : []
  }
}

const service = {
  get: (url:string,options:object) => fetch(getAPI() + url, { ...options,/*credentials: 'include'*/ }).then(handleResponse),
  post: (url:string, body:object,options:object={}) => fetch(getAPI() + url, { ...options,/*credentials: 'include',*/ method: 'POST', body: JSON.stringify(body) }).then(handleResponse),
  put: (url:string, body:object,options:object={}) => fetch(getAPI() + url, { ...options,/*credentials: 'include',*/ method: 'PUT', body: JSON.stringify(body) }).then(handleResponse),
  delete: (url:string,options:object={}) => fetch(getAPI() + url, {...options, /*credentials: 'include',*/ method: 'DELETE' }).then(handleResponse)
}

export async function queryScenarios() {
  const localToken = window.localStorage.getItem('AccessToken')
  return service.get(`/ModelService/scenarios`,{
    headers: { 
      Model: 'Terminal',
      Authorization: `Bearer ${localToken}`
    }
  })
}

export async function getScenario(scenarioId:string) {
  const localToken = window.localStorage.getItem('AccessToken')
  return service.get(`/ModelService/scenarios/${scenarioId}`,{
    headers: { 
      Model: 'Terminal',
      Authorization: `Bearer ${localToken}`
    }
  })
}

export async function createModelScenario(data:object) {
  const localToken = window.localStorage.getItem('AccessToken')
  return service.post("/ModelService/scenarios", data,{
    headers: { 
      Model: 'Terminal',
      Authorization: `Bearer ${localToken}`
    }})
}

export async function queryModelSpecification() {
  const localToken = window.localStorage.getItem('AccessToken')
  return service.get(`/ModelService/specification`,{
    headers: { 
      Model: 'Terminal',
      Authorization: `Bearer ${localToken}`
    }
  })
}

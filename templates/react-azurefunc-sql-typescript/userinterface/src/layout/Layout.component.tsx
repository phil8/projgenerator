import React from 'react'
import Style from '../styles/Layout.module.sass';
import { TokenComponent} from 'msal-token-lib'
import {getMSALConfiguration} from '../config'
let msalConfig = getMSALConfiguration()

export const Menu = (props: any) => {
  return <div className={Style.menu}>Menu|
    <a href="#/configuration">Configuration</a>|
    <a href="#/scenarios">Scenarios</a>|
  </div>
}

export const Layout = (props: any) => {
  return <div className={Style.layoutContainer}>
      <div className={Style.header}>Header
        <TokenComponent config={msalConfig}/>  
      </div>
      <Menu/>   
      {props.children}
      <div className={Style.footer}>Footer</div>
    </div>
}

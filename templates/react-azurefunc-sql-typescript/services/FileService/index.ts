import Debug from 'debug'
import Busboy from 'busboy'
import { AzureFunction, Context, HttpRequest } from "@azure/functions"
import * as fs from 'fs';
import {config} from '../src/config'
//const {config} = require(process.cwd()+'/dist/config')


const debug = Debug('file.service')

//console.log(process.cwd())

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function processed a request: FileService');
        
    if(req.method==='GET'){
        context.res = {
            status: 200,
            body: "Get Handled"
        };
    }else if(req.method==='POST'){

        debug('Upload')    
        var busboy = new Busboy({ headers: req.headers })
        let saveTo = null
        //let uploadFilename
        let fileName = ''
        busboy.on('field', function (fieldname, val) {
        debug(`${fieldname}:${val}`)
        });
        busboy.on('file', async (fieldname, file, filename, encoding, mimetype) => {
        //uploadFilename = filename
        //saveTo = path.join(config.STORAGE_DIRECTORY, filename)
        debug('Uploading: ' + saveTo)
        //   file.pipe(fs.createWriteStream(saveTo)).on('close', () => {
        //     debug('Uploaded')
            //fileService.upsertFile(fileName, saveTo)
        //   })

            file.on('data', function(data) {
                console.log(`File [${fieldname}]: filename: '${filename}', got ${data.length} bytes`);
                fs.writeFileSync(config.storage.directory + filename, data);
            });
            file.on('end', function() {
                console.log(`File [${fieldname}]: filename: '${filename}', Finished`);
            });
        })
        busboy.on('finish', async () => {
            debug(`Finish busboy`)
            context.res = {
                status: 200, 
                body: { result: "done" }
            };
        //   try {
        //     context.res.writeHead(200, { 'Connection': 'close' })
        //     context.res.end()
        //   } catch (err) {
        //     debug(err)
        //     context.res.sendStatus(400).send(err.code)
        //   }
        })
        busboy.write(req.body, function() {});
    }

};

export default httpTrigger;
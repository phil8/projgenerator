import Debug from 'debug'

import {ModelSpecificationService} from '../../application/src/service/ModelSpecificationService'
import {Log} from '../../application/src/persistentlog/specification'
import {getModel as getSpecificationModel} from '../../application/src/model/specification'

import {ModelScenarioService} from '../../application/src/service/ModelScenarioService'
import {getModel as getScenarioModel} from '../../application/src/model/scenario'

// const {ModelSpecificationService} = require(process.cwd()+'/dist/app')
// const {ModelScenarioService} = require(process.cwd()+'/dist/app')
// const {Log} = require(process.cwd()+'/dist/persistentlog/specification')
// const {getModel: getSpecificationModel} = require(process.cwd()+'/dist/model/specification')
// const {getModel: getScenarioModel} = require(process.cwd()+'/dist/model/scenario')

import express from "express"
import passport from 'passport'
import { createAzureFunctionHandler } from "azure-function-express"
import bodyparser from 'body-parser'

import {config} from '../src/config'
import {authorizationStrategy} from '../src/auth'

const debug = Debug('ModelService')

const app = express();    
app.use(bodyparser.json())
app.use(passport.initialize());
passport.use(authorizationStrategy(config));

app.get("/api/ModelService/scenarios/:scenarioId", passport.authenticate('oauth-bearer', { session: false }), async (req, res) => {
    const scenarioModel = await getScenarioModel(config.scenarioDatabase)
    const modelScenarioService = await ModelScenarioService(scenarioModel)
    let scenario = await modelScenarioService.get(req.params.scenarioId)
    res.status(200).send(scenario)
})

app.get("/api/ModelService/scenarios", passport.authenticate('oauth-bearer', { session: false }), async (req, res) => {
    
    const scenarioModel = await getScenarioModel(config.scenarioDatabase)
    const modelScenarioService = await ModelScenarioService(scenarioModel)        
    let scenarios = await modelScenarioService.query()
    res.status(200).send(scenarios)
})

app.post("/api/ModelService/scenarios", passport.authenticate('oauth-bearer', { session: false }), async (req, res) => {
    console.log(req.body)

    const scenarioModel = await getScenarioModel(config.scenarioDatabase)
    const modelScenarioService = await ModelScenarioService(scenarioModel)        
    await modelScenarioService.save(req.body)
    res.status(200).send({})
})

app.get("/api/ModelService/specification", passport.authenticate('oauth-bearer', { session: false }), async (req, res) => {
    
    const model=req.headers.model    

    const specificationModel = await getSpecificationModel(config.specificationDatabase)
    const modelSpecificationService = await ModelSpecificationService(specificationModel)    
    const log = Log(modelSpecificationService)    
    await modelSpecificationService.seed(log)
    
    const id = req.params.id    
    let components = await modelSpecificationService.queryComponents(model)
    res.status(200).send(components)
});

export default createAzureFunctionHandler(app);


import { describe} from 'riteway';
import {ModelScenarioService,ModelSpecificationService} from '../src/service'
import {Log as ScenarioLog} from '../src/persistentlog/scenario'
import {Log as SpecificationLog} from '../src/persistentlog/specification'
import Debug from 'debug'
import { reifyScenario} from '../src/service/UseCaseService'
import {scenarioConfig,specificationConfig} from './testconfig'
import {getModel as getScenarioModel} from '../src/model/scenario'
import {getModel as getSpecificationModel} from '../src/model/specification'

const debug = Debug('test')

describe('Use Case', async assert => {
  try {

    let scenarioModel = await getScenarioModel(scenarioConfig.database)
    const modelScenarioService = await ModelScenarioService(scenarioModel)

    let specificationModel = await getSpecificationModel(specificationConfig.database)
    const modelSpecService = await ModelSpecificationService(specificationModel)

    const speclog = SpecificationLog(modelSpecService)    
    await modelSpecService.seed(speclog)

    const log = ScenarioLog(modelScenarioService)
    await modelScenarioService.seed(log)

    const modelSpec = await modelSpecService.queryComponents("Terminal")

    const spec = await reifyScenario(modelSpec)

    debug(JSON.stringify(spec))
    scenarioModel.close()
    specificationModel.close()
  }catch(ex){
    console.log(ex)
  }

  assert({
    given: 'no arguments',
    should: 'return 0',
    actual: 0,
    expected: 0
  });

})

import { describe} from 'riteway';
import {ModelScenarioService} from '../src/service'
import {Log} from '../src/persistentlog/scenario'
import Debug from 'debug'
import {scenarioConfig as config} from './testconfig'
import {getModel} from '../src/model/scenario'

const debug = Debug('test')

describe.only('Scenario', async assert => {
  let model
  try {

     model = await getModel(config.database)

    const modelService = await ModelScenarioService(model)

    const log = Log(modelService)
    debug(log)
    await modelService.seed(log)

    const spec = await modelService.get(log[0].id)
    debug(JSON.stringify(spec))

    // const scenarios = await modelService.query({})
    // debug(JSON.stringify(scenarios))
    
    
  }catch(ex){
    console.log(ex)
  }

  assert({
    given: 'no arguments',
    should: 'return 0',
    actual: 0,
    expected: 0
  });
  if(model) model.close()

})


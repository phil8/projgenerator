
import { describe} from 'riteway';
import {ModelSpecificationService} from '../src/service'
import {Log} from '../src/persistentlog/specification'
import Debug from 'debug'
import {specificationConfig as config} from './testconfig'
import {getModel} from '../src/model/specification'

const debug = Debug('test')

describe('Specification', async assert => {
  try {
    let model = await getModel(config.database)
    const modelService = await ModelSpecificationService(model)

    const log = Log(modelService)
    debug(log)
    await modelService.seed(log)

    const spec = await modelService.queryComponents('Terminal')

    debug(JSON.stringify(spec))

    model.close()
  }catch(ex){
    console.log(ex)
  }

  assert({
    given: 'no arguments',
    should: 'return 0',
    actual: 0,
    expected: 0
  });

})
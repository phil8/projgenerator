const inmemoryConfig = {
    database: {    
      DBDIALECT: 'sqlite',
      force: true,
      RESYNC: 'true',
      logging: true,
    },
    seed: false
  }
  
  const localSpecificationConfig = {
    database: {    
      USER: "gpc",
      PASSWORD: 'gpc',
      DBHOST: 'localhost',
      DB: 'gpc',
      DBDIALECT: 'mssql',
      schema: 'specification',
      force: true,
      RESYNC: 'true',
      logging: false,
    },
    seed: false
  }
  
  export const specificationConfig = localSpecificationConfig


  const localScenarioConfig = {
    database: {    
      USER: "gpc",
      PASSWORD: 'gpc',
      DBHOST: 'localhost',
      DB: 'gpc',
      DBDIALECT: 'mssql',
      schema: 'scenario',
      force: true,
      RESYNC: 'true',
      logging: true,
    },
    seed: false
  }
  
  export const scenarioConfig = localScenarioConfig
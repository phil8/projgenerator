import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelService')

export const SetItemFactory = async (sequelize:any, config:any) => {
  const SetItem = sequelize.define('SetItems', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },    
    value: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    schema: config.schema,
    tableName: 'SetItems',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await SetItem.sync({ force: config.force }) }

  async function create (data:any) {
      return await SetItem.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return SetItem.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await SetItem.count({ where })
    return result
  }

  async function query (where = {}) {    
    return SetItem.findAll(where)
  }

  async function destroy (id = '') {
    return SetItem.destroy({ where: { id } })
  }


  return {
    SetItem,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}

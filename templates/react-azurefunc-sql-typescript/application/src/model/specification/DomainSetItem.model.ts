import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelService')

export const DomainSetItemFactory = async (sequelize:any, config:any) => {
  const DomainSetItem = sequelize.define('DomainSetItems', {
    domainId: {
      type: DataTypes.STRING,      
    },    
    setItemId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    schema: config.schema,
    tableName: 'DomainSetItems',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await DomainSetItem.sync({ force: config.force }) }

  async function create (data:any) {
    return DomainSetItem.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return DomainSetItem.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await DomainSetItem.count({ where })
    return result
  }

  async function query (where = {}) {    
    return DomainSetItem.findAll(where)
  }

  async function destroy (id = '') {
    return DomainSetItem.destroy({ where: { id } })
  }

  return {
    DomainSetItem,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}

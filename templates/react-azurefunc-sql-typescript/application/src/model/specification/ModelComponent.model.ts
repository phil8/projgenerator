import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelService')

export const ModelComponentFactory = async (sequelize:any, config:any) => {
  const ModelComponent = sequelize.define('ModelComponents', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    model: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    collection: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    collectionType: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    root: {
      type: DataTypes.STRING,
      allowNull: true,
    }
  },
  {
    schema: config.schema,
    tableName: 'ModelComponents',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await ModelComponent.sync({ force: config.force }) }

  async function create (data:any) {
    debug(data)
    return ModelComponent.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return ModelComponent.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await ModelComponent.count({ where })
    return result
  }

  async function query (where = {}) {
    debug('Query')
    return ModelComponent.findAll({where, include: [{ all: true, nested: true }]})
  }

  async function destroy (id = '') {
    return ModelComponent.destroy({ where: { id } })
  }

  return {
    ModelComponent,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}

import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelService')

export const ParameterDomainFactory = async (sequelize:any, config:any) => {
  const ParameterDomain = sequelize.define('ParameterDomains', {
    domainId: {
      type: DataTypes.STRING,
      primaryKey: true,
    },    
    parameterId: {
      type: DataTypes.STRING,
      allowNull: false,
    },    
  },
  {
    schema: config.schema,
    tableName: 'ParameterDomains',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await ParameterDomain.sync({ force: config.force }) }

  async function create (data:any) {
    return ParameterDomain.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return ParameterDomain.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await ParameterDomain.count({ where })
    return result
  }

  async function query (where = {}) {    
    return ParameterDomain.findAll(where)
  }

  async function destroy (id = '') {
    return ParameterDomain.destroy({ where: { id } })
  }

  return {
    ParameterDomain,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}

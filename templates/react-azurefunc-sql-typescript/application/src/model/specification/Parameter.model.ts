import { Sequelize,DataTypes  } from 'sequelize'

//export const DOMAIN_TYPE = 'Domain'

export const ParameterFactory = async (sequelize:any, config:any) => {
  const Parameter = sequelize.define('Parameters', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },    
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    configurable: {
      type: DataTypes.STRING
    },
    referenceType: {
      type: DataTypes.STRING
    },
  },
  {
    schema: config.schema,
    tableName: 'Parameters',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await Parameter.sync({ force: config.force }) }

  async function create (data:any) {
    return Parameter.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return Parameter.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await Parameter.count({ where })
    return result
  }

  async function query (where = {}) {
    return Parameter.findAll(where)
  }

  async function destroy (id = '') {
    return Parameter.destroy({ where: { id } })
  }

  return {
    Parameter,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}

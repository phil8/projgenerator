import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelService')

export const DomainFactory = async (sequelize:any, config:any) => {
  const Domain = sequelize.define('Domains', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },    
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    domainType: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    defaultValue: {
      type: DataTypes.STRING,
      allowNull: true,
    }
  },
  {
    schema: config.schema,
    tableName: 'Domains',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await Domain.sync({ force: config.force }) }

  async function create (data:any) {
    return Domain.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return Domain.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await Domain.count({ where })
    return result
  }

  async function query (where = {}) {    
    return Domain.findAll(where)
  }

  async function destroy (id = '') {
    return Domain.destroy({ where: { id } })
  }


  return {
    // CONSTANT_TYPE,
    // RANGE_TYPE,
    // SET_TYPE,
    // DISTRIBUTION_TYPE,
    // DISTRIBUTIONSET_TYPE,

    Domain,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}

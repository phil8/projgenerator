import Debug from 'debug'
import { Sequelize } from 'sequelize'
const debug = Debug('service')
import { ModelComponentFactory }  from './ModelComponent.model'
import { ParameterFactory }  from './Parameter.model'
import { DomainFactory }  from './Domain.model'
import { SetItemFactory }  from './SetItem.model'

import { ParameterDomainFactory }  from './ParameterDomain.model'
import { ModelComponentParameterFactory }  from './ModelComponentParameter.model'
import { DomainSetItemFactory }  from './DomainSetItem.model'


export const ModelFactory = async (config:any)=>{
    debug('Model Factory')    
    debug(config)

  const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
    host: config.DBHOST,
    dialect: config.DBDIALECT,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    dialectOptions: {
      encrypt: true,
    },
    logging: config.logging ? console.log : false,

    // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    // operatorsAliases: false
    // timezone: 'utc'
  })

  const ModelComponent = await ModelComponentFactory(sequelize, config)
  const Parameter = await ParameterFactory(sequelize, config)
  const Domain = await DomainFactory(sequelize, config)
  const SetItem = await SetItemFactory(sequelize, config)

  const DomainSetItem = await DomainSetItemFactory(sequelize, config)
  const ParameterDomain = await ParameterDomainFactory(sequelize, config)
  const ModelComponentParameter = await ModelComponentParameterFactory(sequelize, config)

  ModelComponent.ModelComponent.hasMany(ModelComponentParameter.ModelComponentParameter)
  ModelComponentParameter.ModelComponentParameter.belongsTo(Parameter.Parameter)
  
  Parameter.Parameter.hasMany(ParameterDomain.ParameterDomain)
  ParameterDomain.ParameterDomain.belongsTo(Domain.Domain)

  Domain.Domain.hasMany(DomainSetItem.DomainSetItem)
  DomainSetItem.DomainSetItem.belongsTo(SetItem.SetItem)
  
  function close () {
    sequelize.close()
  }

  return {
    SetItem,
    ModelComponent,
    Parameter,
    Domain,
    ParameterDomain,
    ModelComponentParameter,
    DomainSetItem,
    close,
  }
}



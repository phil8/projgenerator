import {ModelFactory} from './ModelFactory'
import {ISpecificationModel} from '../../types'
import Debug from 'debug'

const debug = Debug('model')

let model :ISpecificationModel|null = null

export const getModel = async (config:any) : Promise<ISpecificationModel> => {
    if(!model){        
        debug('Building ModelFactory')
        model = await ModelFactory(config)
    }
    return model
}

export * from './ModelFactory'
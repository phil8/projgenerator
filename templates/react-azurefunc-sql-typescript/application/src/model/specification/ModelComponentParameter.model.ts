import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelService')

export const ModelComponentParameterFactory = async (sequelize:any, config:any) => {
  const ModelComponentParameter = sequelize.define('ModelComponentParameters', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
      allowNull: false,
    },
    modelComponentId: {
      type: DataTypes.STRING,
    },    
    parameterId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    schema: config.schema,
    tableName: 'ModelComponentParameters',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await ModelComponentParameter.sync({ force: config.force }) }

  async function create (data:any) {
    return ModelComponentParameter.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return ModelComponentParameter.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await ModelComponentParameter.count({ where })
    return result
  }

  async function query (where = {}) {    
    return ModelComponentParameter.findAll(where)
  }

  async function destroy (id = '') {
    return ModelComponentParameter.destroy({ where: { id } })
  }

  return {
    ModelComponentParameter,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}

import {ModelFactory} from './ModelFactory'
import {IScenarioModel} from '../../types'
import Debug from 'debug'

const debug = Debug('model')

let model :IScenarioModel|null = null

export const getModel = async (config:any) : Promise<IScenarioModel> => {
    if(!model){
        debug('Building ModelFactory')
        model = await ModelFactory(config)
    }
    return model
}

export * from './ModelFactory'
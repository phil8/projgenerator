import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelComponentInstanceFactory')

export const ModelComponentInstanceFactory = async (sequelize:any, config:any) => {
  const ModelComponentInstance = sequelize.define('ModelComponentInstances', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    scenarioId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    modelComponentId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    
  },
  {
    schema: config.schema,
    tableName: 'ModelComponentInstances',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await ModelComponentInstance.sync({ force: config.force }) }

  async function create (data:any) {
    return ModelComponentInstance.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function upsert (data:any) {
    return ModelComponentInstance.upsert(data)
  }

  async function get (id = '') {
    const where = { id }
    return ModelComponentInstance.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await ModelComponentInstance.count({ where })
    return result
  }

  async function query (where = {}) {
    debug('Query')
    return ModelComponentInstance.findAll({where, 
      //include: [{ all: true, nested: true }]
    })
  }


  async function destroy (id = '') {
    return ModelComponentInstance.destroy({ where: { id } })
  }

  return {
    ModelComponentInstance,
    create,
    update,
    upsert,
    get,
    query,
    queryCount,
    destroy,
  }
}

import Debug from 'debug'
import { Sequelize } from 'sequelize'
const debug = Debug('service')
import { ScenarioFactory }  from './Scenario.model'
import { ModelComponentInstanceFactory }  from './ModelComponentInstance.model'
import { ParameterInstanceFactory }  from './ParameterInstance.model'
import { DomainValueFactory }  from './DomainValue.model'

export const ModelFactory = async (config:any)  =>{
    debug('Model Factory')    
    debug(config)

  const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
    host: config.DBHOST,
    dialect: config.DBDIALECT,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    dialectOptions: {
      encrypt: true,
    },
    logging: config.logging ? console.log : false,

    // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    // operatorsAliases: false
    // timezone: 'utc'
  })

  const Scenario = await ScenarioFactory(sequelize, config)
  const ModelComponentInstance = await ModelComponentInstanceFactory(sequelize, config)
  const ParameterInstance = await ParameterInstanceFactory(sequelize, config)
  const DomainValue = await DomainValueFactory(sequelize, config)
  
  //Scenario.Scenario.hasMany(ModelComponentInstance.ModelComponentInstance)
  //ModelComponentInstance.ModelComponentInstance.belongsTo(Scenario.Scenario)
  
  //ModelComponentInstance.ModelComponentInstance.hasMany(ParameterInstance.ParameterInstance)
  //ParameterInstance.ParameterInstance.belongsTo(ModelComponentInstance.ModelComponentInstance)

  //ParameterInstance.ParameterInstance.hasMany(DomainValue.DomainValue)
  //DomainValue.DomainValue.belongsTo(ParameterInstance.ParameterInstance)
  
  function close () {
    sequelize.close()
  }

  return {
    Scenario,
    ModelComponentInstance,
    ParameterInstance,
    DomainValue,
    close
  }
}



import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ScenarioFactory')

export const ScenarioFactory = async (sequelize:any, config:any) => {
  const Scenario = sequelize.define('Scenarios', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    model: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },    
  },
  {
    schema: config.schema,
    tableName: 'Scenarios',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await Scenario.sync({ force: config.force }) }

  async function create (data:any) {
    return Scenario.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function upsert (data:any) {
    return Scenario.upsert(data)
  }

  async function get (id = '') {
    const where = { id }        
    return Scenario.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await Scenario.count({ where })
    return result
  }

  async function query (where = {}) {
    debug('Query')
    return Scenario.findAll({where, include: [{ all: true, nested: true }]})
  }

  async function queryScenarios (where = {}) {
    debug('Query Scenario')
    return Scenario.findAll({where})
  }

  async function destroy (id = '') {
    return Scenario.destroy({ where: { id } })
  }

  return {
    Scenario,
    create,
    upsert,
    update,
    get,
    query,
    queryScenarios,
    queryCount,
    destroy,
  }
}

import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ParameterInstanceFactory')

export const ParameterInstanceFactory = async (sequelize:any, config:any) => {
  const ParameterInstance = sequelize.define('ParameterInstances', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    modelcomponentinstanceId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    parameterId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    schema: config.schema,
    tableName: 'ParameterInstances',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await ParameterInstance.sync({ force: config.force }) }

  async function create (data:any) {
    return ParameterInstance.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function upsert (data:any) {
    return ParameterInstance.upsert(data)
  }

  async function get (id = '') {
    const where = { id }
    return ParameterInstance.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await ParameterInstance.count({ where })
    return result
  }

  async function query (where = {}) {
    debug('QUery')
    return ParameterInstance.findAll({where, 
      //include: [{ all: true, nested: true }]
    })
  }

  async function destroy (id = '') {
    return ParameterInstance.destroy({ where: { id } })
  }

  return {
    ParameterInstance,
    create,
    update,
    upsert,
    get,
    query,
    queryCount,
    destroy,
  }
}

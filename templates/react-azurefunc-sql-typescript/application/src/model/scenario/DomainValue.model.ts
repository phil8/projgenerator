import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('DomainValueFactory')

export const DomainValueFactory = async (sequelize:any, config:any) => {
  const DomainValue = sequelize.define('DomainValues', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    parameterinstanceId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    domainId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    value: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    schema: config.schema,
    tableName: 'DomainValues',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await DomainValue.sync({ force: config.force }) }

  async function create (data:any) {
    return DomainValue.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function upsert (data:any) {
    return DomainValue.upsert(data)
  }

  async function get (id = '') {
    const where = { id }
    return DomainValue.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await DomainValue.count({ where })
    return result
  }

  async function query (where = {}) {
    debug('QUery')
    return DomainValue.findAll({where, 
      //include: [{ all: true, nested: true }]
    })
  }

  async function destroy (id = '') {
    return DomainValue.destroy({ where: { id } })
  }

  return {
    DomainValue,
    create,
    update,
    upsert,
    get,
    query,
    queryCount,
    destroy,
  }
}

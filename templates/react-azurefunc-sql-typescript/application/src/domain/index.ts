
//Scenario
export const SCENARIO_TYPE='Scenario'
export const MODELCOMPONENTINSTANCE_TYPE='ModelComponentInstance'
export const PARAMETERINSTANCE_TYPE='ParameterInstance'
export const DOMAINVALUE_TYPE='DomainValue'

//Specification
export const SETITEM_TYPE='SetItem'
export const DOMAIN_TYPE='Domain'
export const PARAMETER_TYPE='Parameter'
export const COMPONENT_TYPE='Component'
export const PARAMETERDOMAIN_TYPE='ParameterDomain'
export const MODELCOMPONENTPARAMETER_TYPE='ModelComponentParameter'
export const DOMAINSETITEM_TYPE='DomainSetItem'


//Domain Type
export const CONSTANT_TYPE='constant'
export const RANGE_TYPE='range'
export const SET_TYPE='set'
export const DISTRIBUTION_TYPE='distribution'
export const DISTRIBUTIONSET_TYPE='distributionset'

// Parameter Types
export const PARAMETER_DOMAIN_TYPE='Domain'
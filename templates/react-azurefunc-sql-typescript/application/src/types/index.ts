
// Specification

export interface ISpecificationModelService {    
    model: any,
    seed: any,
    create: any,                
    queryComponents: any    
}

export interface ISpecificationModel {
    ModelComponent: any,
    Domain: any,
    Parameter: any,
    SetItem: any,
    ModelComponentParameter: any,
    ParameterDomain: any,
    DomainSetItem: any,
    close: any
}

export interface IModelComponent {
    id: string,
    model: string,
    name: string,
    description: string,
    collection: string,
    collectionType?: string,
    root: string,
    parameters?: IParameter[]
}

export interface IParameter {
    id: string,
    name: string,
    description: string,
    referenceType?: string
    domains?: IDomain[]
}

export interface IDomain {
    id: string,
    name: string,
    type: string,
    domainType: string,
    defaultValue?: string
}

export interface ISetItem {
    id: string,
    value: string
}

// Scenario

export interface IScenarioModelService {
    model: any,
    seed: any,
    save: any,

    create: any,
    get:any,
    query: any,
}

export interface IScenarioModel {
    Scenario: any,
    ModelComponentInstance: any,
    ParameterInstance: any,
    DomainValue: any,
    close: any
}


export interface IScenario {
    type: string,
    id: string,
    name: string,
    model: string,
}


export interface IModelComponentInstance {
    type: string,
    id: string,
    scenarioId: string,
    modelComponentId: string,
}

export interface IParameterInstance {
    type: string,
    id: string,
    parameterId: string,
    modelcomponentinstanceId: string,
}

export interface IDomainValue {
    type: string,
    id: string,
    parameterinstanceId: string,
    domainId: string,
    value: string,
}
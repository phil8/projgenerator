import {IScenarioModelService,IScenarioModel, IModelComponentInstance, IParameterInstance, IDomainValue} from '../types'
import {SCENARIO_TYPE,MODELCOMPONENTINSTANCE_TYPE,PARAMETERINSTANCE_TYPE,DOMAINVALUE_TYPE} from '../domain'

let seeded = false

export const ModelScenarioService = async (model: IScenarioModel) : Promise<IScenarioModelService> => {
   
    const get = async (id:string)=>{        
        const scenario = await model.Scenario.get(id)
        const scenarioObj = scenario.toJSON()
        scenarioObj.type='Scenario'
        const modelComponentInstances = await model.ModelComponentInstance.query({scenarioId: id})
        const modelComponentInstanceObjs = modelComponentInstances.map((mci:any)=>{
            const mciObj = mci.toJSON()
            mciObj.type='ModelComponentInstance'
            return mciObj
        })
        const parameterInstances = await model.ParameterInstance.query({modelComponentInstanceId: [modelComponentInstances.map((mci:IModelComponentInstance)=>mci.id)]})

        const parameterInstanceObjs = parameterInstances.map((pi:any)=>{
            const piObj = pi.toJSON()
            piObj.type='ParameterInstance'
            return piObj
        })
        const domainValues = await model.DomainValue.query({parameterInstanceId: [parameterInstances.map((p:IParameterInstance)=>p.id)]})
        const domainValueObjs = domainValues.map((dv:any)=>{
            const dvObj = dv.toJSON()
            dvObj.type='DomainValue'
            return dvObj})
        return [
            scenarioObj,
            ...modelComponentInstanceObjs,
            ...parameterInstanceObjs,
            ...domainValueObjs
        ]
    }

    const query = async (where:any)=>{
        const relationalModel = await model.Scenario.queryScenarios(where)
        return relationalModel
    }

    const create = (scenario:any)=>{        
        if(scenario.type===SCENARIO_TYPE){
            return model.Scenario.upsert(scenario)
        }else if(scenario.type===MODELCOMPONENTINSTANCE_TYPE){
             return model.ModelComponentInstance.upsert(scenario)
        }else if(scenario.type===PARAMETERINSTANCE_TYPE){
            return model.ParameterInstance.upsert(scenario)
        }else if(scenario.type===DOMAINVALUE_TYPE){
            return model.DomainValue.upsert(scenario)
        }
    }

    const save = async (log:any) => {        
        for(var i=0;i<log.length;i++){
            try {
                await create(log[i])
            }catch(ex){
            console.error("Unable to create")
            console.log(ex)
            console.error(log[i])
            }
        }
    }

    const seed = async (log:any) => {
        if(!seeded){
            await save(log)            
            seeded=true
        }
    }

 
    return {
        model,
        seed,
        save,

        create,
        get,
        query
    } 
}


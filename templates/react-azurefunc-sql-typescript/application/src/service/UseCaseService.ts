import { IModelComponent, IParameter, IDomain, IModelComponentInstance, IParameterInstance,IDomainValue  } from "../types"
import { v4 as uuid} from 'uuid'

import { SCENARIO_TYPE, MODELCOMPONENTINSTANCE_TYPE, PARAMETERINSTANCE_TYPE, DOMAINVALUE_TYPE} from '../domain'

const collectionSizeDefault =(modelcomponent:IModelComponent) => {        
    let collectionSizeParameter = modelcomponent.parameters?.find((p:IParameter)=>{        
        return p.name==='collectionSize'
    })    
    let domains = collectionSizeParameter?.domains    
    return domains && domains.length===1 ? domains[0].defaultValue : 1
}

const getModelComponent = (specification:IModelComponent[],modelcomponentName:string) => {
    return specification.find((mc: IModelComponent)=>mc.name===modelcomponentName)
}

export const reifyScenario = (specification:any)=>{    

    const scenarioId = uuid()
    const scenario = {
        type: SCENARIO_TYPE,
        id:scenarioId,
        model: 'terminal',
        name: `Scenario-${scenarioId}`
      }
    
    const createModelComponentInstance = (name:string,modelcomponent:IModelComponent)=> {
        const modelcomponentinstance =  {
            type: MODELCOMPONENTINSTANCE_TYPE,
            id:uuid(),
            name,
            scenarioId: scenarioId,
            modelComponentId: modelcomponent.id
        }
        let parameterInstances:any =[]
        if(modelcomponent.parameters){            
            const pms: any = modelcomponent.parameters
            parameterInstances = pms.flatMap((parameter:IParameter)=>{
                return createParameterInstance(modelcomponentinstance,parameter)
            })
        }        
        return [modelcomponentinstance,...parameterInstances]
        
    }
    const createParameterInstance = (modelcomponentinstance:IModelComponentInstance, parameter: IParameter) => {      

        const parameterinstance = {
            type: PARAMETERINSTANCE_TYPE,
            id:uuid(),
            name: parameter.name,
            modelcomponentinstanceId: modelcomponentinstance.id,
            parameterId: parameter.id
        }        
        let domainValues:IDomainValue[]  = []
        if(parameter.domains && parameter.domains.length>0){
            domainValues = [createDomainValue(parameterinstance,parameter.domains[0])]        
        }
        return [parameterinstance,...domainValues]

    }
    const createDomainValue = (parameterinstance:IParameterInstance, domain: IDomain) : IDomainValue => {
        return {
            type: DOMAINVALUE_TYPE,
            id:uuid(),            
            parameterinstanceId: parameterinstance.id,
            domainId: domain.id,
            value: domain.defaultValue || ''
        }
    }
    
    let components = specification.flatMap((modelcomponent: IModelComponent)=>{
        let results:any =[]
        if(modelcomponent.root==='true'){
            if(modelcomponent.collection==='0'){
                results = results.concat(createModelComponentInstance(modelcomponent.name,modelcomponent))
            }else if(modelcomponent.collection==='1' && modelcomponent.collectionType){
                results = results.concat(createModelComponentInstance(modelcomponent.name,modelcomponent))
                const collectionModelComponent = getModelComponent(specification, modelcomponent.collectionType )
                if(collectionModelComponent){
                    let collectionSize = collectionSizeDefault(modelcomponent)
                    if(collectionSize){                        
                        for(let i=0;i<collectionSize;i++){
                            results = results.concat(createModelComponentInstance(collectionModelComponent.name+i,collectionModelComponent))
                        }
                    }
                }
            }
        }
        return results
    })
    return [scenario,...components]
}
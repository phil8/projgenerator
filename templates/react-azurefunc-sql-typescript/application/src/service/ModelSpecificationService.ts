import {ISpecificationModelService,ISpecificationModel} from '../types'
import Debug from 'debug'
const debug = Debug('ModelSpecificationService')
import {SETITEM_TYPE,DOMAINSETITEM_TYPE,COMPONENT_TYPE,DOMAIN_TYPE,PARAMETER_TYPE,PARAMETERDOMAIN_TYPE,MODELCOMPONENTPARAMETER_TYPE} from '../domain'

let seeded = false

export const ModelSpecificationService = async (model : ISpecificationModel) : Promise<ISpecificationModelService> => {
    
    const queryComponents = async (modelInUse:string)=>{    
        const relationalModel = await model.ModelComponent.query({model: modelInUse})

        const setitemModel = ((compparam:any)=>{
            const param = compparam.SetItem
            return {
                value: param.value                
            }
        })

        const domainModel = ((compparam:any)=>{
            const domain = compparam.Domain            
            return {                
                id: domain.id,
                name: domain.name,
                domainType: domain.domainType,
                description: domain.description,
                defaultValue: domain.defaultValue,
                setitems: domain.DomainSetItems.map(setitemModel)
            }
        })
        const parameterModel = ((compparam:any)=>{
            const param = compparam.Parameter
            return {                
                id: param.id,
                name: param.name,
                description: param.description,
                domains: param.ParameterDomains.map(domainModel)
            }
        })
        const componentModel = ((component:any)=>{
            return {                
                id: component.id,
                name: component.name,
                description: component.description,
                collection: component.collection,
                collectionType: component.collectionType,
                root: component.root,
                parameters: component.ModelComponentParameters.map(parameterModel)
            }
        })
        return relationalModel.map(componentModel)
    }

    const create = (specification:any)=>{
        debug(specification)
        if(specification.type===SETITEM_TYPE){
            return model.SetItem.create(specification)
        }else if(specification.type===DOMAINSETITEM_TYPE){
            return model.DomainSetItem.create(specification)
        } else if(specification.type===COMPONENT_TYPE){
            return model.ModelComponent.create(specification)
        }else if(specification.type===DOMAIN_TYPE){
            return model.Domain.create(specification)
        }else if(specification.type===PARAMETER_TYPE){
            return model.Parameter.create(specification)        
        }else if(specification.type===PARAMETERDOMAIN_TYPE){
            return model.ParameterDomain.create(specification)
        }else if(specification.type===MODELCOMPONENTPARAMETER_TYPE){            
            return model.ModelComponentParameter.create(specification)
        }
    }

    const seed = async (log:any) => {
        if(!seeded){
            for(var i=0;i<log.length;i++){
                try {
                await create(log[i])
                }catch(ex){
                console.error("Unable to create")
                console.log(ex)
                console.error(log[i])
                }
            }
            seeded=true
        }
    }
    

    return {
        model,
        seed,

        create,
                
        queryComponents
    } 
}


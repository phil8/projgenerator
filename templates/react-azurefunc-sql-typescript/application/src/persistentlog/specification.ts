import { IModelComponent, IParameter, IDomain, ISetItem} from '../types'
//import { SET_TYPE,RANGE_TYPE,DISTRIBUTION_TYPE,DISTRIBUTIONSET_TYPE} from '../domain'
import {CONSTANT_TYPE,SET_TYPE,RANGE_TYPE,DISTRIBUTION_TYPE,DISTRIBUTIONSET_TYPE,
  
  SETITEM_TYPE,
  DOMAINSETITEM_TYPE,
  COMPONENT_TYPE,
  DOMAIN_TYPE,
  PARAMETER_TYPE,
  PARAMETERDOMAIN_TYPE,
  MODELCOMPONENTPARAMETER_TYPE,

  PARAMETER_DOMAIN_TYPE
} from '../domain'

let nextId = 0
const id = ()=>{
  return ''+nextId++
}

export const Log = (modelService:any)=>{


  const domainSetItem = (domain: IDomain, setitem: ISetItem) => {
    return{
      id: id(),
      type: DOMAINSETITEM_TYPE,      
      domainId: domain.id,
      setItemId: setitem.id,
    }
  }
  
  const modelComponentParameter = (modelcomponent: IModelComponent, parameter: IParameter) => {
    return{
      id: id(),
      type: MODELCOMPONENTPARAMETER_TYPE,      
      modelComponentId: modelcomponent.id,
      parameterId: parameter.id,
    }
  }
  const parameterDomain = (parameter: IParameter,domain: IDomain, ) => {
    return{
      id: id(),
      type: PARAMETERDOMAIN_TYPE,
      domainId: domain.id,
      parameterId: parameter.id,
    }
  }
  
  
  const portExample = ()=>{
  //Example of port variables
  // numberOfTugs
  // numberOfParameters

  // channelDepth
  // speedRestrictions 
  return []
  } 

  const terminalExample = ()=>{

    const domainNumberOfShipLoaders = {
      type: DOMAIN_TYPE,
      id:id(),
      name: 'Number of Ship Loaders',      
      description: 'Number of Ship Loaders domain',    
      domainType: CONSTANT_TYPE,
      defaultValue: '2'
    }
    const domainFailureRate = {
      type: DOMAIN_TYPE,
      id:id(),
      name: 'Failure Rate',      
      description: 'Failure Rate',    
      domainType: CONSTANT_TYPE,
      defaultValue: '10'
    }
    const domainRange = {
      type: DOMAIN_TYPE,
      id:id(),
      name: 'Range',      
      description: 'Range domain',
      domainType: RANGE_TYPE,
    }
    const domainDistribution = {
      type: DOMAIN_TYPE,
      id:id(),
      name: 'Distribution',
      description: 'Distributio domain',
      domainType: DISTRIBUTION_TYPE,
    }
    const domainDistributionSet = {
      type: DOMAIN_TYPE,
      id:id(),
      name: 'DistributionSet',
      description: 'Distribution Set domain',
      domainType: DISTRIBUTIONSET_TYPE,
    }

    const domainOnOff = {
      type: DOMAIN_TYPE,
      id:id(),
      name: 'On Off',      
      description: 'OnOff',    
      domainType: SET_TYPE,
      defaultValue: 'On'
    }

    const OnItem = {
      id:id(),
      type: SETITEM_TYPE,
      value: 'On',
    }
    const OffItem = {
      id:id(),
      type: SETITEM_TYPE,
      value: 'Off',
    }

    const componentShipLoaders = {
      id: id(),
      type: COMPONENT_TYPE,
      model: "Terminal",
      name: 'ShipLoaders',
      description: 'ShipLoaders',      
      collection: '1',
      collectionType: 'ShipLoader',
      root: 'true'
    }
    const componentShipLoader = {
      id: id(),
      type: COMPONENT_TYPE,
      model: "Terminal",
      name: 'ShipLoader',
      description: 'ShipLoader',
      collection: '0',
      root: 'false'
    }
    const componentConveyor = {
      id: id(),
      type: COMPONENT_TYPE,
      model: "Terminal",
      name: 'Conveyor',
      description: 'Conveyor',
      collection: '0',
      root: 'true'
    }
    const componentRailDepot = {
      id: id(),
      type: COMPONENT_TYPE,
      model: "Terminal",
      name: 'Rail Depot',
      description: 'Rail Depot',
      collection: '0',
      root: 'true'
    }

    const parameterConveyorUpgrade = {
      type: PARAMETER_TYPE,
      parameterType: PARAMETER_DOMAIN_TYPE,
      id: id(),
      name: 'ConveyorUpgrade',
      description: 'ConveyorUpgrade',
      class: "Conveyor"
    }
    const parameterConveyorFailureRate = {
      type: PARAMETER_TYPE,
      parameterType: PARAMETER_DOMAIN_TYPE,
      id: id(),
      name: 'ConveyorFailureRate',
      description: 'Conveyor Failure Rate',
      class: "Conveyor"
    }

    const parameterTrainArrival = {
      type: PARAMETER_TYPE,
      parameterType: PARAMETER_DOMAIN_TYPE,
      id: id(),
      name: 'TrainArrival',
      description: 'Train Arrival',
      class: "RailDepot"
    }

    const parameterNumberOfShipLoaders = {
      type: PARAMETER_TYPE,
      parameterType: PARAMETER_DOMAIN_TYPE,
      id: id(),
      name: 'collectionSize',
      description: 'Number of ShipLoaders'
    }
    const parameterSpeed = {
      type: PARAMETER_TYPE,
      parameterType: PARAMETER_DOMAIN_TYPE,
      id: id(),
      name: 'Speed',
      description: 'Speed '
    }

    const range100 = {
        type: DOMAIN_TYPE,
        domainType: RANGE_TYPE,
        id: id(),
        name: 'Range 0-100',
        description: 'Range 0-100'
    }
  
    const equipmentX = {
      id: id(),
      type: COMPONENT_TYPE,
      name: 'Equipment X',
      description: 'Equipment X',
    }
    return [

      domainNumberOfShipLoaders,
      domainFailureRate,
      domainRange,
      domainDistribution,
      domainDistributionSet,

      OnItem,
      OffItem,
      domainOnOff,

      componentShipLoaders,
      componentShipLoader,
      componentConveyor,
      componentRailDepot,

      parameterNumberOfShipLoaders,      
      parameterSpeed,
      parameterConveyorUpgrade,
      parameterConveyorFailureRate,
      parameterTrainArrival,
         
      domainSetItem(domainOnOff,OnItem),
      domainSetItem(domainOnOff,OffItem),

      parameterDomain(parameterConveyorFailureRate,domainDistribution),
      parameterDomain(parameterConveyorFailureRate,domainFailureRate),
      parameterDomain(parameterConveyorUpgrade,domainOnOff),
      parameterDomain(parameterNumberOfShipLoaders,domainNumberOfShipLoaders),
      parameterDomain(parameterSpeed,domainRange),
      parameterDomain(parameterTrainArrival,domainDistributionSet),

      modelComponentParameter(componentShipLoaders,parameterNumberOfShipLoaders),
      modelComponentParameter(componentShipLoader,parameterSpeed),
      modelComponentParameter(componentConveyor,parameterConveyorUpgrade),
      modelComponentParameter(componentConveyor,parameterConveyorFailureRate),
      modelComponentParameter(componentRailDepot,parameterTrainArrival)
      ]
    }

      return [
        //...componentReferenceComponentExample(),
        //...portExample(),
        ...terminalExample()
      ]
}
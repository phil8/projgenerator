import { IScenarioModelService} from '../types'
import {SCENARIO_TYPE, MODELCOMPONENTINSTANCE_TYPE,PARAMETERINSTANCE_TYPE,DOMAINVALUE_TYPE} from '../domain'

let nextId = 0
const id = ()=>{
  return ''+nextId++
}

export const Log = (modelService:IScenarioModelService)=>{

  const Scenario1 = {
    type: SCENARIO_TYPE,
    id:id(),
    model: 'terminal',
    name: 'Scenario1'    
  }

  const ShipLoader1 = {
    type: MODELCOMPONENTINSTANCE_TYPE,
    id:id(),
    scenarioId: Scenario1.id,
    modelComponentId: 1,
    name: 'mci-ShipLoader1'   
  }

  const ShipLoaders1 = {
    type: MODELCOMPONENTINSTANCE_TYPE,
    id:id(),
    scenarioId: Scenario1.id,
    modelComponentId: 2,
    name: 'mci-ShipLoaders1'   
  }


  const NumberOfShipLoaders1 = {
    type: PARAMETERINSTANCE_TYPE,
    id:id(),
    modelcomponentinstanceId: ShipLoaders1.id,
    parameterId: 1,
    name: 'pi-NumberOfShipLoaders'
  }

  const NumberOfShipLoadersValue1 = {
    type: DOMAINVALUE_TYPE,
    id:id(),
    parameterinstanceId: NumberOfShipLoaders1.id,
    domainId: 1,
    value: '2'    
  }

  return [
    Scenario1,
    ShipLoader1,
    ShipLoaders1,

    NumberOfShipLoaders1,

    NumberOfShipLoadersValue1
  ]  
}

import time
import threading

from app.service.processlock import ProcessLock
lock =  ProcessLock()

def sendResponse():
    print("Placehodler for sending async response")

def processJob(config):
    print("Job Processing")
    lock.startJob()
    #print(config)
    time.sleep(10)
    print("Job Processed")
    lock.endJob()
    sendResponse()

def submitJob(config):
    print( not lock.job_in_progress)
    if lock.allowJob() :
        t1 = threading.Thread(target=processJob, args=(config,))
        t1.start()
        return {'status':'Job Submitted'}
    else:
        return {'status':'Job Not Submitted, Existing Job In Progress'}


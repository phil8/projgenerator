
class ProcessLock:
    job_in_progress = False
    def allowJob(self):
        return not self.job_in_progress
    def startJob(self):
        self.job_in_progress = True
    def endJob(self):
        self.job_in_progress = False
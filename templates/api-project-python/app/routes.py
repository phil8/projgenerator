from flask import jsonify
from app import app

from app.service import core    

# K8S Health Check
@app.route('/')
def index():
    print("Health Check")
    return 'success'

@app.route('/jobs',methods=['POST'])
def submitJob():
    print("Route Submitting-Job")
    config = {'chemical': 'hydrogen'}
    results =  core.submitJob(config)
    return jsonify(results)
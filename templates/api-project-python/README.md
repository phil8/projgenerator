## {{{projectname}}} Web Application

## About

This is a flask based python web application.

# Prerequisites

`python3` and `pipenv` are requirements

# Install

```
pipenv install --python=/usr/bin/python3
```

## Setup

```
export FLASK_ENV=development
export FLASK_APP=app.py
```

# Usage

Serve application
```
pipenv run python -m flask run 
```

# Test

```
pipenv run pytest
```

or using the pipefile script as
```
pipenv run test
```


## References

[How to creating python web application](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world)
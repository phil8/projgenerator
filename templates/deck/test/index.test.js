import { describe } from 'riteway';

describe('Sample Test', async (assert) => {
  assert({
    given: 'something',
    should: 'something else happen',
    actual: true,
    expected: true
  });
});

import React from 'react';
import { render} from 'react-dom';
import * as themes from 'deck/src/themes/index.js'
import { components }  from 'deck/src/gatsbyplugin/index.js'

import Content from './index.md'
const { SlideDeck } = components

const App = (props) => {
    return  <SlideDeck
                location={location}
                theme={themes.aurecon}
                navigate={()=>{}}
                components={components}
                //animationMode='rotate'
                children={Content({components}).props.children}
            >
            </SlideDeck>
}

render(<App />, document.querySelector('#presentation'));


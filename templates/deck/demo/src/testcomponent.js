import React from 'react'

export const  TestComponent = () => {

    const [count,setCount] = React.useState(0)

    const handleClick=()=>{
        setCount(count+1)
    }
    
    return <div>
        Count {count}

        <button onClick={handleClick}>Click</button>

    </div>
}

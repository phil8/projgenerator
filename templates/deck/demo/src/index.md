import { TestComponent } from './testcomponent.js'
import { Steps , Invert, Split, SplitRight ,Image, AnimateSelect, ThemeSelect } from 'deck/src/gatsbyplugin/components.js'
import { images } from './images'

# What is 
***[MDX DECK](https://github.com/jxnblk/mdx-deck#readme)***

----
# Motivations

I wanted to be able to
- have ***self contained*** html files
- containing presentation content
- and ***react*** components including ***d3*** and ***web gl***





---
# What markdown is supported

## Bullets
* Bullet1
* Bullet2

## Tables
| Fruit         | Weight   | Cost  |
| ------------- | ---------| ----- |
| Strawberries  | 16oz     | $3.99 |
| Apples | 16oz     | $4.99 |

## Block Quote
> This is the quote

## Inline

```This is some code```

## Links
[Components](https://mdxjs.com/table-of-components)

## Image
<Image src={images.UseCases}  width={200} height={200}/>





---
# MDX Deck Features
<Steps>
<li>React components</li>
<li>Presentation Modes</li>
<li>Steps</li>
<li>Themes</li>
<li>Splits</li>
<li>Invert</li>
<li>Print to PDF</li>
</Steps>




---
# React Components

<TestComponent/>





---
# Presentation Mode

<Steps>
<li>With Grid Mode</li>
<li>With Presentation Mode</li>
<li>With Overview Mode</li>
</Steps>




---
# Themes

<ThemeSelect/>

Themes can be easily added.
Uses [theme-ui](https://theme-ui.com/getting-started) which I need to learn more about.



---
# View Themes

## Bullets
* Bullet1
* Bullet2

## Tables
| Fruit         | Weight   | Cost  |
| ------------- | ---------| ----- |
| Strawberries  | 16oz     | $3.99 |




---
# Invert

<Invert>

Some content that should be interesting to read 

<Image src={images.UseCases}  width={200} height={200}/>

with some description

</Invert>





---
# Split up the display

<Split>

<Image src={images.UseCases}  width={200} height={200}/>

Some content which should be displayed
</Split>


<SplitRight>

<Image src={images.UseCases}  width={200} height={200}/>

Else    

</SplitRight>






---
# Features I was able to add

## Animations

<AnimateSelect/>

There are a whole lot more at [react-animation](https://www.npmjs.com/package/react-animations) to add in

---
# Self Contained Html

I wanted to make self contained html.
```
npm run prod
```

---
# Next Steps

Add it into the project generator

---
# Base Technologies


- [mdxjs.com](https://mdxjs.com/)
- [mdx-deck](https://www.diycode.cc/projects/jxnblk/mdx-deck)
- [theme ui](https://theme-ui.com/getting-started)
- [styled components](https://styled-components.com/)

- [WebPack](https://webpack.js.org/)
- [Tagged Template Literals](https://medium.com/@trukrs/tagged-template-literal-for-html-templates-4820cf5538f9)


---
<Header>
<h2>Lunch and Learn</h2>
</Header>

<Footer>
<h2>MDX Deck</h2>
</Footer>

Thanks


# Presentation {{{projectname}}}

## Setup

```npm install```

## Develop

```npm start```

start browser at http://localhost:3002

## Produce self contained html

```npm run prod```

Please open the self contained html in `demo/dist`

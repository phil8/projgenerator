import React from 'react';
import { render} from 'react-dom';


import { Model} from 'real-value-lang'
import Debug from 'debug'
import {produceData,produceControl,produceDisplay} from '../../src'
import ContainerComponent from 'real-value-react-gauge-component'

let model = Model()

//Data Source Events
let citec = model.from([{production: 1},{production: 3},{production: 5}]).delay(5000)
let corvus = model.from([{target: 100,measure: 'stockpile1',time: 2}])
let wenco = model.from([{status: 'off',asset: 'digger-1', time: 4}])

let architecture = produceData({
    model,
    source_target: corvus, 
    source_production: citec,
    source_asset: wenco,
})

architecture = produceControl(architecture)
architecture = produceDisplay(architecture)

const { performanceDisplay } = architecture

const App = () => performanceDisplay;

render(<App />, document.getElementById("root"));

model.run()
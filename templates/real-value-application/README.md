# Real-Value Application Template     

## About

This is an application template for dashboard application which requires
processing data from multiple sources
and a user interface that provides real time display and control.

## Install Dependencies


```
npm install 
```

## How to use

### A browser only demonstration
```
npm run demo
```

### A browser and server only demonstration
```
npm run demo-client-server
```




import { Model} from 'real-value-lang'
import Debug from 'debug'
import {produceData} from '../../src'
import { ChannelFactory} from 'real-value-channel-socketio-server'

let debug= Debug('demo')

var app = require('express')();
var server = require('http').Server(app);

const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const config = require('../webpack.config.js');
const compiler = webpack(config);

app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
  }));
  
server.listen(9000,()=>{debug('Listening')});

let channelFactory = ChannelFactory({server})

let model = Model({channelFactory})

//Data Source Events
let citec = model.from([{production: 1},{production: 3},{production: 5},{production: 3},{production: 1}]).delay(5000).log()
let corvus = model.from([{target: 100,measure: 'stockpile1',time: 2}])
let wenco = model.from([{status: 'off',asset: 'digger-1', time: 4}])

let architecture = produceData({
    model,
    source_target: corvus, 
    source_production: citec,
    source_asset: wenco,
})

architecture.stream_production.toChannel({name: 'citec'})
architecture.stream_target.toChannel({name:'corvus'})
architecture.stream_asset.toChannel({name:'wenco'})

model.run()
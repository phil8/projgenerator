import React from 'react';
import { render} from 'react-dom';

import { Model} from 'real-value-lang'
import {produceControl,produceDisplay} from '../../src'
import { ChannelFactory  } from 'real-value-channel-socketio-client' 

let channelFactory = ChannelFactory("http://localhost:9000")

let model = Model({channelFactory})

const corvus = model.fromChannel({name:'corvus'})
const citec = model.fromChannel({name:'citec'})
const wenco = model.fromChannel({name:'wenco'})

let architecture = {
    model,
    stream_target: corvus, 
    stream_production: citec,
    stream_asset: wenco,
}

architecture = produceControl(architecture)
architecture = produceDisplay(architecture)

const { performanceDisplay } = architecture 

const App = () => performanceDisplay;

render(<App />, document.getElementById("root"));

model.run()

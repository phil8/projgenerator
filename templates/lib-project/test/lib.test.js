import Debug from 'debug'
import { describe } from 'riteway'
import { someFunction } from '../index.js'
const debug = Debug('test')


describe('Some Function', async (assert) => {

  let scenarios = [
    {
        given: 'someFunction',
        should: 'return',
        expected: "hello"
    }
  ]

  for(const index in scenarios){
    let scenario = scenarios[index]
    
    let result= someFunction()
    assert({
      given: scenario.given,
      should: scenario.should,
      actual: result,
      expected: scenario.expected
    })
  }
})

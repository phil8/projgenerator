import React, { useReducer } from 'react'
import { HierarchyComponent } from './hierarchy-component'
import { DependencyComponent } from './dependency-component'
import { CSVDownloadComponent } from './csvdownload-component'
import { LayerHierarchyComponent } from './layerhierarchy-component'
import { array,links } from './model/Task'
import { ZoneFilterFactory } from './processor/componentfilter'

import './styles.css'

export const WBSView = ({hierarchyName,data})=>{
  return <HierarchyComponent name={hierarchyName} data = {data} />
}

export const DependencyView = ({dependencyName,dependencyNodes,dependencyLinks})=> {
  return <DependencyComponent name={dependencyName} nodes={dependencyNodes} links={dependencyLinks}/> 
}

export const SecurityView = ({securityRelationships,metadata})=>{
  return <LayerHierarchyComponent name="layers" mapdata={securityRelationships} metadata={metadata}/> 
}

export const SecurityDownload = ({link,securityRows})=>{
  return <CSVDownloadComponent link={link} data={securityRows} labels={['parent','id','name','type','comment','question','permissions','zone']} props={[x=>{return x.parent? x.parent.id : ''},'id','name','type','comment','question','permissions','zone']} /> 
} 

export const WSBDownload = ({link,taskRows})=>{
  return <CSVDownloadComponent link={link} data={taskRows}
  labels={['Level 1','Level 2','Level 3','Name','Comment','Question','Permissions','Status']}
  props={[ x=>x.depth===2 ? x.parent.id : '',x=>x.depth===3 ? x.parent.id : '', x=>x.depth===4 ? x.parent.id : '','name','comment','question','permissions' ,'status' ]}/> 
} 


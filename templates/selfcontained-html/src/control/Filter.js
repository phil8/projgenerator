
import XFilter from 'crossfilter2'

export const Filter =()=>{

    let filter = {
        xfilter: XFilter(),
        dimensions: {},
        dimensionGroups: {}
    }
    filter.Dimension = (name,dimensionFunction=null)=>{
        if(dimensionFunction!=null){
            filter.dimensions[name] = filter.xfilter.dimension(dimensionFunction)
            filter.dimensionGroups[name]= filter.dimensions[name].group()
            return filter
        }else {
            return filter.dimensions[name]
        }
    }
    filter.DimensionGroup = (name)=>{
        return filter.dimensionGroups[name]
    }
    filter.Data = (records)=>{
        filter.xfilter.add(records)
        return filter
    }

    return filter
}
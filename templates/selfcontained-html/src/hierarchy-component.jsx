import React, { useEffect, useState } from 'react';

import { format } from 'd3-format'
import {select} from 'd3-selection'
import { hierarchy, partition } from 'd3-hierarchy'
import {scaleOrdinal} from 'd3-scale'
import {schemeCategory10} from 'd3-scale-chromatic'
import {transition} from 'd3-transition'

export const HierarchyComponent =  ({ name,data }) => {

  const [theVis, setVis] = useState(null);
  let thevis = theVis
  let setup = () => {
	if(!thevis){
		thevis = visualization(`#${name}`, {
			//size: 300,
		});
		thevis.render();
		setVis(thevis)
	}
	thevis.update(data)
  }

  var visualization = function(container, configuration) {
	var that = {};
	var config = {
		width: 1800,
		height: 600

	};

	const color = scaleOrdinal(schemeCategory10)

	function configure(configuration) {
		var prop = undefined;
		for ( prop in configuration ) {
			config[prop] = configuration[prop];
		}
		
	}
	that.configure = configure;

	const formdata = data => {
		const root = hierarchy(data)
			.sum(d => d.value)
			//.sort((a, b) => b.height - a.height || b.value - a.value);  
		return partition().size([config.height, (root.height + 1) * config.width / 3])(root);
	}
	
	function render(newValue) {
		that.svg = select(container)
			.append('svg:svg')
				.attr('class', 'visualization')
				.attr('width', config.width)
				.attr('height', config.height)
				.attr('viewBox', [0, 0, config.width, config.height])
				.style('font', '16px sans-serif');
		
				const root = formdata(data);
				let focus = root;
			
				const desc = root.descendants()
				const cell = that.svg
					.selectAll('g')
					.data(desc)
					.join('g')
					.attr('transform', d => `translate(${d.y0},${d.x0})`);
			
				const rect = cell.append('rect')
					.attr('width', d => d.y1 - d.y0 - 1)
					.attr('height', d => rectHeight(d))
					.attr('fill-opacity', 0.6)
					.attr('fill', d => {
						if (!d.depth) return '#ccc';
						while (d.depth > 1) d = d.parent;
						return color(d.data.name);
					})
					.style('cursor', 'pointer')
					.on('click', clicked);

				const status = cell.append('circle')
					.attr('cx', 5)
					.attr('cy', 5)
					.attr('r',5)
					.attr('fill-opacity', 0.75)
					.attr('fill', d => {
						if(d.data.status==='Done') return 'Blue'
						if(d.data.status==='In Progress') return 'Black'
						if(d.data.status==='Available') return 'Green'
						if(d.data.status.startsWith('Blocked')) return 'Red'
						return 'Grey'
					})

				const text = cell.append('text')
					.style('user-select', 'none')
					.attr('pointer-events', 'none')
					.attr('x', 10)
					.attr('y', 13)
					// .attr("fill-opacity", d => +labelVisible(d));
	
				const tspan_name = text.append('tspan')
					.attr('opacity', d => rectHeight(d)> 20 ? 1 : 0)
					.text(d => d.data.name);

				const tspan = text.append('tspan')
					.style('font', '12px sans-serif')
					.attr('opacity', d => rectHeight(d)> 20 ? 1 : 0)
					.text(d => d.value >1 ? '\t\t\t-->' : '')

					
				const tspanType = text.append('tspan')
					.attr('dy','1em')
					.attr('x','10')
					.attr('opacity', d => rectHeight(d)> 20 ? 1 : 0)
					.text(d => {
						return d.data.type ? `Type: ${d.data.type}`: ''
					});
				
				const tspanComment = text.append('tspan')
					.attr('dy','1em')
					.attr('x','10')
					.style('font', '12px sans-serif')
					.attr('opacity', d => rectHeight(d)> 20 ? 1 : 0)
					.text(d => {
						return d.data.comment ? d.data.comment: ''
					});
				
				const tspanQuestion = text.append('tspan')
					.attr('dy','1em')
					.attr('x','10')
					.style('font', '12px sans-serif')
					.attr('opacity', d => rectHeight(d)> 20 ? 1 : 0)
					.text(d => {
						return d.data.question ? d.data.question: ''
					});
				
				const tspanImage = cell.append('g').attr('transform', d => `translate(600,0)`).append('image').attr('height',d=>600).attr('href',d => { return d.data.image ? d.data.image: ''})
			
				const spanPermissions = text.append('tspan')
					.attr('dy','1em')
					.attr('x','10')
					.style('font', '12px sans-serif')
					.attr('opacity', d => rectHeight(d)> 20 ? 1 : 0)
					.text(d => {
						return d.data.permissions ? `Permission: ${d.data.permissions}`: ''
					});

				cell.append('title')
					.attr('class','label')
					.text(d => `${d.ancestors().map(d => d.data.name).reverse().join('/')}\n${d.data.status}\n${d.data.comment}\n${d.data.question ? d.data.question: ''}`); //\n${format(d.value)}
	
				function clicked(p) {
					focus = focus === p ? p = p.parent : p;
					root.each(d => d.target = {
					x0: (d.x0 - p.x0) / (p.x1 - p.x0) * config.height,
					x1: (d.x1 - p.x0) / (p.x1 - p.x0) * config.height,
					y0: d.y0 - p.y0,
					y1: d.y1 - p.y0
				});
			
				const t = cell.transition().duration(750).attr('transform', d => `translate(${d.target.y0},${d.target.x0})`);
			
					rect.transition(t).attr('height', d => rectHeight(d.target));
					text.transition(t).attr('fill-opacity', d => labelVisible(d.target));
					tspan_name.transition(t).attr('opacity', d=> rectHeight(d.target)> 20 ? 1 : 0) 
					tspan.transition(t).attr('opacity', d=> rectHeight(d.target)> 20 ? 1 : 0) 
					tspanType.transition(t).attr('opacity', d=> rectHeight(d.target)> 20 ? 1 : 0)
					tspanComment.transition(t).attr('opacity', d=> rectHeight(d.target)> 20 ? 1 : 0)
					tspanQuestion.transition(t).attr('opacity', d=> rectHeight(d.target)> 20 ? 1 : 0)
					tspanImage.transition(t).attr('opacity', d=> rectHeight(d.target)> 100 ? 1 : 0)
					//tspanPermissions.transition(t).attr('opacity', d=> rectHeight(d.target)> 20 ? 1 : 0)
				}
				
				function rectHeight(d) {
					return d.x1 - d.x0 - Math.min(1, (d.x1 - d.x0) / 2);
				}
			
				function labelVisible(d) {
					return d.y1 <= config.width && d.y0 >= 0 && d.x1 - d.x0 > 16;
				}

			
		update(newValue === undefined ? 0 : newValue);
	}
	that.render = render;
	
	function update(newValue, newConfiguration) {
		if ( newConfiguration  !== undefined) {
			configure(newConfiguration);
		}
		//transition`
	}
	that.update = update;

	configure(configuration);
	
	return that;
  };

  useEffect(setup)
  
  return <div id={name}></div>
}





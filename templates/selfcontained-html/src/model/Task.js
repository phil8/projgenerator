
// export const array = (hierarchy)=>{
//     let answer = []
//     function processChildren(node){
//         answer.push(node)
//         node.Children().forEach((n)=>{processChildren(n)})
//     }
//     processChildren(hierarchy)
//     return answer
// }

// export const links = (hierarchy)=>{
//     let answer = []
//     function processChildren(node){
//         let refs = node.references.map(r=>({source: node.id,target: r.id}))
//         answer = answer.concat(refs)
//         node.Children().forEach((n)=>{processChildren(n)})
//     }
//     processChildren(hierarchy)
//     return answer
// }

export const Tasks = (identifier)=>{
    
    let nodes = {}

    let dependencyNodes = []
    let dependencyLinks = []
    
    let realizations = null
    
    const Task = (identifier,theparent=null)=>{

        let task = {
            depth: theparent ? theparent.depth+1 : 0,
            id: identifier,
            name: identifier,
            type:'',
            comment:'',
            question:null,
            permissions: null,
            children : [],
            parent : theparent,
            references : [],
            value: 1,
            isReference: '',
            realization: null,
            status: 'Pending',
            zone: '',
            image: ''
        }
    
        nodes[task.id] = task
        task.DependencyNodes =()=>Object.keys(dependencyNodes).map(x=>dependencyNodes[x])
        task.DependencyLinks = ()=>dependencyLinks
        task.Depth = () =>task.depth
        task.Node = (name)=>nodes[name]
        task.Identifier= ()=>task.id,  
        task.Done= ()=>task.parent ? task.parent : task
        task.Children = ()=>task.children
        task.Name= (n)=>{
            if(!n) return task.name
            else task.name=n
            return task
        },
        task.Status= (n)=>{
            if(!n) return task.status
            else task.status=n
            return task
        },
        task.Zone= (n)=>{
            if(!n) return task.zone
            else task.zone=n
            return task
        },
        task.Type= (t)=>{
            if(!t) return task.type
            else task.type=t.replace(' ','')
            return task
        }
        task.Comment= (c)=>{
            if(!c) return task.comment
            else task.comment=c
            return task
        },
        task.Image= (c)=>{
            if(!c) return task.image
            else task.image=c
            return task
        },
        task.Question= (q)=>{
            if(!q) return task.question
            else task.question=q
            return task},
        task.Link= (c)=>{
            if(!c) return task.link
            else task.link=c
            return task
        },
        task.Permissions= (p)=>{
            if(!p) return task.permissions
            else task.permissions=p
            return task
        },
        task.Realizations = (tasks)=>{
            realizations = tasks
            return task
        }
        task.Realize= (ref)=>{
            task.realization = realizations.Node(ref)
            return task
        },
        // task.Realize=(ref)=>{
        //     return task.ref(ref,'realization')
        // }
        task.Ref= (ref)=>{
            return task.ref(ref,'Dependency')
        },
        task.ref= (ref,type)=>{
            task.references.push(nodes[ref])
    
            task.isReference=task.id

            if(!nodes[ref]){
                console.log(`Bad ref[${ref}]`) // eslint-disable-line no-console
            }
            nodes[ref].isReference=ref
    
            dependencyNodes[task.name]=task
            dependencyNodes[ref]=nodes[ref]
    
            dependencyLinks.push({source:task.id,target:ref,type})
            
            return task
        },
        
        
        task.$ = (identifier)=>{
            let child = Task(identifier,task)
            task.children.push(child)
            return child
        }
        return task
    }

    return Task(identifier,null)
}

export const Dimension = (type,tasks)=>{

    const nodes = {}
    const links = []
    
    const Relation = (parent,data)=>{

        if(data)
            nodes[data.id]=data
        let dimension = {
            parent,
            data
        }

        dimension.$ = (identifier)=>{
            const refData = tasks.Node(identifier)
            if(refData){
                if(data)
                    links.push({source: data.id,target: refData.id, type: 'Dependency'})
                let d = Relation(dimension,refData)
                return d
            }else {
                console.log(`Bad ref ${identifier}`) // eslint-disable-line no-console
            }
        }
        dimension.Nodes =()=>Object.keys(nodes).map(x=>nodes[x])
        dimension.Links = ()=>links
        
        dimension.Done= ()=>dimension.parent ? dimension.parent : dimension

        return dimension
    }
    let dim = Relation(null,null)
    return dim

}
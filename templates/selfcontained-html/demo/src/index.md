import UC from './UseCases.png'
import { Image} from 'deck/src/gatsbyplugin/components.js'
import dataProp from './dataprep'
import { WBSView, SecurityView, DependencyView, WSBDownload, SecurityDownload } from '../../src'

#  Manawatu Gorge 

## Architecture Overview
---
# Work Breakdown

<WBSView hierarchyName="hier" {...dataProp} />

<WSBDownload link="Download Task Data" {...dataProp} />

---
# Dependency View

<DependencyView dependencyName="Dep" {...dataProp} />

---
# Security View

<SecurityView {...dataProp} />

<SecurityDownload link="Download Task Data" {...dataProp} />

---
# Slide 1
import { TestComponent } from './testcomponent'

<div style={{ padding: '10px 30px', backgroundColor: 'green' }}>
  <h2>Try making this heading have the color green</h2>
</div>

<TestComponent/>

```something```

<code>foo bar</code>

<Image src={UC} width={600} height={500}/>

---
<Header>
<h2>Aurecon</h2>
</Header>

<Footer>
<h3 >Aurecon</h3>
</Footer>
import { ZoneFilterFactory } from '../../src/processor/componentfilter'
import { tasks } from '../../src/data/tasks'
//import { array } from '../../src/model/Task'
//import { Filter } from '../../src/control/Filter'
import { securitycontrols } from '../../src/data/securitycontrols'

const DataPrep = () => {
  
    let securitycontrolsArray = securitycontrols.Nodes()
  
    let zone1 = securitycontrolsArray.filter(x=>x.zone==='UntrustedZone')
    let zone2 = securitycontrolsArray.filter(x=>x.zone==='ApplicationZone')
    let zone3 = securitycontrolsArray.filter(x=>x.zone==='DataZone')
    let zone4 = securitycontrolsArray.filter(x=>x.zone==='AdminZone')
  
    var zone1Filter = ZoneFilterFactory(zone1);
    var zone2Filter = ZoneFilterFactory(zone2);
    var zone3Filter = ZoneFilterFactory(zone3);
    var zone4Filter = ZoneFilterFactory(zone4);
  
    let data = tasks
  
    let dependencyNodes = tasks.DependencyNodes() 
    let taskRows = tasks.DependencyNodes().filter(x=>x.depth>=2 && x.depth<=4)
    let dependencyLinks = tasks.DependencyLinks()
    let securityRows = securitycontrols.Nodes()
    let securityRelationships = securitycontrols.Links()
    
    var metadata = [
      {
        key: 'zone1',
        transform: 0,
        scale: 0.1,
        filter: zone1Filter.crossFilter,
        fixed: true,
        class: 'zone1'
      },
      {
        key: 'zone2',
        transform: 0.2,
        scale: 0.2,
        filter: zone2Filter.crossFilter,
        class: 'zone2'
      },
      {
        key: 'zone3',
        transform: 0.4,
        scale: 0.2,
        filter: zone3Filter.crossFilter,
        class: 'zone3'
      },
      {
        key: 'zone4',
        transform: 0.6,
        scale: 0.2,
        filter: zone4Filter.crossFilter,
        class: 'zone4'
      },
    ]
    return {
      metadata,
      data,
      dependencyNodes,
      dependencyLinks,
      taskRows,
      securityRows,
      securityRelationships
    }
  }

  export default DataPrep()
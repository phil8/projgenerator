import { describe } from 'riteway';
var assert = require('assert')
var debug=require('debug')('test')
var { computeDependencyHierarchy: cph } = require('../src/processor/compute-dependency-hierarchy.js');

var nodes= [
    {
      id: 'A'
    },
    {
      id: 'B'
    },
]

var relationships = [
  {
    'source': 'A',
    'target': 'B',
  }
]

var layername = 'layer';

describe('Component Dependency Hierarchy', async (assert) => {
      var layers = cph(layername,nodes,relationships)
      debug(layers)
      assert({
        given: '2 Nodes and 1 relationship',
        should: 'should produce 2 separate layers for A depends upon B',
        actual: `${layers.length} ${layers[0].filter((n)=>n.id==layername+0).length} ${layers[1].filter((n)=>n.id==layername+1).length} ${layers[0].filter((n)=>n.layerdepth==0 && n.id=='A').length} ${layers[0].filter((n)=>n.layerdepth==0 && n.parent==layername+0).length} ${layers[1].filter((n)=>n.layerdepth==1 && n.parent==layername+1).length}`,
        expected: '2 1 1 1 1 1'
      })
})

var nodes2= [
    {
      id: 'A'
    },
    {
      id: 'B'
    },
    {
      id: 'C'
    },
]

var relationships2 = [
  {
    'source': 'A',
    'target': 'B',
  },
  {
    'source': 'B',
    'target': 'C',
  }
]


describe('Component Dependency Hierarchy', async (assert)=> {
      var layers = cph(layername,nodes2,relationships2)
      debug(JSON.stringify(layers));
      
      assert({
        given: '3 Nodes and 2 relationship',
        should: 'should produce 2 separate layers for A depends upon B which depends on C',
        actual: `${layers.length} ${layers[0].filter((n)=>n.id==layername+0).length} ${layers[1].filter((n)=>n.id==layername+1).length} ${layers[2].filter((n)=>n.id==layername+2).length} ${layers[0].filter((n)=>n.layerdepth==0 && n.id=='A').length} ${layers[1].filter((n)=>n.layerdepth==1 && n.id=='B').length} ${layers[2].filter((n)=>n.layerdepth==2 && n.id=='C').length}`,
        expected: '3 1 1 1 1 1 1'
        });
});


var nodes3= [
    {
      id: 'A'
    },
    {
      id: 'B'
    },
    {
      id: 'C'
    },
]

var relationships3 = [
  {
    'source': 'A',
    'target': 'B',
  },
  {
    'source': 'C',
    'target': 'A',
  }
]


describe('Component Dependency Hierarchy', async (assert)=>{
      var layers = cph(layername,nodes3,relationships3)
      debug(JSON.stringify(layers));

      assert({
        given: '3 Nodes and 2 relationship',
        should: 'should produce 2 separate layers for C depends upon A which depends on B',
        actual: `${layers.length} ${layers[0].filter((n)=>n.id==layername+0).length} ${layers[1].filter((n)=>n.id==layername+1).length} ${layers[2].filter((n)=>n.id==layername+2).length} ${layers[0].filter((n)=>n.layerdepth==0 && n.id=='C').length} ${layers[1].filter((n)=>n.layerdepth==1 && n.id=='A').length} ${layers[2].filter((n)=>n.layerdepth==2 && n.id=='B').length}`,
        expected: '3 1 1 1 1 1 1'
        });
});


var nodes4= [
    {
      id: 'A'
    },
    {
      id: 'B'
    },
    {
      id: 'C'
    },
]


var relationships4 = [

  {
    'source': 'A',
    'target': 'B',
  },
  {
    'source': 'C',
    'target': 'A',
  },
  {
    'source': 'B',
    'target': 'C',
  }
]

describe('Component Dependency Hierarchy', async (assert)=>{
      var layers = cph(layername,nodes4,relationships4)
      debug(JSON.stringify(layers));

      assert({
        given: '3 Nodes and 3 relationship',
        should: 'should produce 2 separate layers for A depends upon B which depends on C',
        actual: `${layers.length} ${layers[0].filter((n)=>n.id==layername+0).length} ${layers[1].filter((n)=>n.id==layername+1).length} ${layers[2].filter((n)=>n.id==layername+2).length} ${layers[0].filter((n)=>n.layerdepth==0 && n.id=='B').length} ${layers[1].filter((n)=>n.layerdepth==1 && n.id=='C').length} ${layers[2].filter((n)=>n.layerdepth==2 && n.id=='A').length}`,
        expected: '3 1 1 1 1 1 1'
        });
});


var nodes5= [
    {
      id: 'Z'
    },
    {
      id: 'A'
    }
]


var relationships5 = [
]

describe('Component Dependency Hierarchy', async (assert)=> {
      var layers = cph(layername,nodes5,relationships5)
      debug(JSON.stringify(layers));

      assert({
        given: '2 Nodes and 0 relationship',
        should: 'should produce 1 layer',
        actual: `${layers.length} ${layers[0].filter((n)=>n.id==layername+0).length} ${layers[0].filter((n)=>n.layerdepth==0 && n.id=='Z').length} ${layers[0].filter((n)=>n.layerdepth==0 && n.id=='A').length}`,
        expected: '1 1 1 1'
        });

});

var nodes6= [
  {
    id: 'A'
  },
  {
    id: 'B'
  }
]


var relationships6 = [
  {
    'source': 'Y',
    'target': 'Z',
  },
]

describe('Component Dependency Hierarchy', async (assert)=>{
    var layers = cph(layername,nodes6,relationships6)
    debug(JSON.stringify(layers));

    assert({
      given: '2 Nodes and 2 separate relationship',
      should: 'should produce 1 layer',
      actual: `${layers.length} ${layers[0].filter((n)=>n.id==layername+0).length} ${layers[0].filter((n)=>n.layerdepth==0 && n.id=='A').length} ${layers[0].filter((n)=>n.layerdepth==0 && n.id=='B').length}`,
      expected: '1 1 1 1'
      });

});
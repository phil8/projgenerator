import { describe } from 'riteway';
import { Tasks,Dimension } from '../src/model/Task'

describe('Task Model', async (assert) => {

  let tasks = Tasks('Tasks')
  tasks.$('Test')
        .$('A').Done()
        .$('B')
          .$('i')
            .Ref('B')
          .Done()
        .Done()
        .$('C')
        .Done()
      .Done()

  let dnodes = tasks.DependencyNodes()
  let dlinks = tasks.DependencyLinks()

  assert({
    given: 'tasks',
    should: 'be convertible into table.',
    actual: `${dnodes.length} ${dlinks.length}`,
    expected: '2 1'
  });

  let dimension = Dimension('dependency',tasks)
  let droot = dimension
    .$('A')
      .$('B')
      .Done()
      .$('C')
      .Done()

  let rnodes = droot.Nodes()
  let rlinks = droot.Links()
    
  assert({
    given: 'tasks',
    should: 'be convertible into table.',
    actual: `${rnodes.length} ${rlinks.length}`,
    expected: '3 2'
  });
});


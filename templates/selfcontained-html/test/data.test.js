import { describe } from 'riteway';
import { array } from '../src/model/Task'
import { tasks } from '../src/data/tasks';
import { securitycontrols } from '../src/data/securitycontrols';

describe('Array', async (assert) => {

  //console.log(table) 

  let table = tasks.DependencyNodes()

  assert({
    given: 'tasks',
    should: 'be convertible into table.',
    actual: table.length>0,
    expected: true
  });
});

describe('SecurityControls', async (assert) => {

    let nodes = securitycontrols.RelationshipNodes()
    let links = securitycontrols.RelationshipLinks()

    assert({
      given: 'security controls',
      should: 'be convertible into table.',
      actual: true,
      expected: true
    });
});
